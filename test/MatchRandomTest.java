package competitionTest;
import org.junit.Before;
import org.junit.Test;
import src.competition.Competitor;
import src.competition.MatchRandom;


public class MatchRandomTest {
    private Competitor c1;
    private Competitor c2;
    private MatchRandom matchRandom;

    @Before
    public void init(){
        this.c1 = new Competitor("EQUIPE1");
        this.c2 = new Competitor("EQUIPE2");
        this.matchRandom = new MatchRandom();
    }

    @Test
    public void ifWinnerPointChangeAfterCallPlayMatch(){
        int sumPointBeforePlayMatch = this.c1.getPoints() + this.c2.getPoints();
        this.matchRandom.playMatch(c1, c2);
        assertEquals(sumPointBeforePlayMatch+1, c1.getPoints()+c2.getPoints());
    }
}
