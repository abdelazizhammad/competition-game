/**
 * 
 */
package test;
import src.competition.*;
import java.util.*;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.*;
/**
 * @author Abdelaziz HAMMAD
 *
 */
public abstract class MasterStrategyTest{
	

    protected Strategy MasterStrategy;

    protected abstract MasterStrategy createMasterStrategy(List<Competitor>competitorList);

    @Test
    public void throwNumberOfCompetitorsNotAchievedException(){
        List<Competitor> competitors = new ArrayList<>();
        this.MasterStrategy = this.createMasterStrategy(competitors);
        this.MasterStrategy.divideIntoGroups(competitors);
    }





}
