package test;
import java.util.List;
import src.competition.Competition;
import src.competition.Competitor;
import src.competition.League;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public abstract class LeagueTest extends CompetitionTest{
    @Override
    protected Competition createCompetition(List<Competitor>competitors) {
        return new League(competitors);
    }
}
