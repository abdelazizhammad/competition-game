package test;
import java.util.List;
import src.competition.Competition;
import src.competition.Competitor;
import src.competition.Tournament;
import org.junit.*;
import static org.junit.*;

public abstract class TournamentTest extends CompetitionTest{
    protected Competition createCompetition(List<Competitor> competitorsList) {
        return new Tournament(competitorsList);
    }
}
