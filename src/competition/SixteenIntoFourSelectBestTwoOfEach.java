package src.competition;
import java.util.List;

public class SixteenIntoFourSelectBestTwoOfEach extends SixteenIntoFourSelectBestOfEach {
    private final int WINNERSOFGROUP = 2;
    public SixteenIntoFourSelectBestTwoOfEach() {
    }

    /**
     * @param group
     * @return
     */
    @Override
    public List<Competitor> selectGroupWiners(List<Competitor> group) {
        return selectCompetitors(group, WINNERSOFGROUP);
    }

}
