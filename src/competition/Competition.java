package src.competition;
import static src.util.MapUtil.sortByDescendingValue;
import java.util.*;



/**
 * abstract Competition class
 */


public abstract class Competition {
    protected Match match;
    protected List<Competitor>competitorList;
    protected Map<Competitor, Integer>competitors = new HashMap<>();
    protected static List<CompetitionObserver> observerList = new ArrayList<>();;

    
    /**
     *constructor of a competition
     * @param competitorList
     */
    
    
    public Competition(List<Competitor> competitorList) {
        this.competitorList = competitorList;
        this.match = new MatchRandom();
    }
    /**
     * start the competition
     *
     */
    /**
     * @throws EmptyCompetitorListException
     * @throws ListSizeIsNotPowerOfTwoException for later verion when Tournament will be implemented
     */
    public void play() throws EmptyCompetitorListException, ListSizeIsNotPowerOfTwoException {
        play(this.competitorList);
    }

    /**
     * allow to play all the match according to type of competition
     *
     * @param competitors list of competitors   
     *
     */
    protected abstract void play(List<Competitor>competitors)throws EmptyCompetitorListException, ListSizeIsNotPowerOfTwoException;

    /**
     * to play a Match
     *
     * @param competitor1 first competitor
     * @param competitor2 second competitor
     */
    protected void playMatch(Competitor competitor1, Competitor competitor2){
        this.match.playMatch(competitor1, competitor2);
        displayWinner(competitor1, competitor2);
    }

    /**
     * sort the competitors by their points
     *
     * @return a Map of the competitor sorted by their points
     */
    public Map<Competitor, Integer> ranking(){
        return sortByDescendingValue(this.competitors);
    }

    
    /**
     * display the winner
     * @param c1 competitor one
     * @param c2 competitor two
     */
    private void displayWinner(Competitor c1, Competitor c2){
        if(c1.getPoints() > c2.getPoints()){
            watchMatch(c1,c2,c1);
            System.out.println(  c1.toString()+" vs "+c2.getName()+ " --- THE WINNER IS :  "+c1.getName());
        }
        else{
            watchMatch(c1,c2,c1);
            System.out.println(c1.toString()+" vs "+c2.getName()+ " --- THE WINNER IS :  "+c2.getName());

        }
        
    }
    

    public void displayResults() {
		Map<Competitor, Integer> res = this.ranking();
		System.out.println(" Ranking ");
		for (Competitor c : res.keySet()) {
	           System.out.println(c.getName() + " - " + c.getPoints());
	    }
	}

    public void resetCompetitorsPoint(List<Competitor>competitorList){
        competitorList.forEach(Competitor::resetPoints);
    }

    /**
     * classification
     */
    public abstract void classification();


   
    
    /**
     * return true if competitor's list size is power of 2, false else
     * @param competitorsList
     * @return tue or false
     */
    //public abstract boolean isPowerOfTwo(List<Competitor>competitorsList);
    public boolean isPowerOfTwo(List<Competitor> competitorsList) {
        return (((float)competitorList.size()/2)%2)==0.0;
    }

    /**
     * add observer to the list of observers
     * @param observer
     */
    public void addObserver(CompetitionObserver observer){
        observerList.add(observer);
    }

    /**
     * remove observer from the list of observers
     * @param observer
     */
    public void removeObserver(CompetitionObserver observer){
        observerList.remove(observer);
    }

        /**
     *  Apply the specific method watchMatch to the list of observers
     * @param c1 Competitor
     * @param c2 Competitor
     * @param winner Competitor 
     */
    public void watchMatch(Competitor c1, Competitor c2, Competitor winner){
        observerList.forEach(observer -> {
            observer.watchMatch(c1, c2, winner);
        });
    }
}






