package src.competition;
import java.util.*;


/**
 * 
 */
public class Master extends Competition {

    /**
     * 
     */
    private MasterStrategy strategy ;
    private List<Competitor> finalPlayers;

    /**
     * List of Winners from groups Round/phase
     */
    private List<Competitor> groupePhaseWiners;
    /**
     * To store groups phase teams
     */
    public Map<Integer, List<Competitor>> groups;

    /**
     * @param competitors 
     * @param MasterStrategy
     */
    public Master(List<Competitor> competitors, MasterStrategy masterstrategy) {
        super(competitors);
        this.strategy = masterstrategy;
        this.groups = new HashMap<>();
        this.groupePhaseWiners = new ArrayList<>();
        this.finalPlayers = new ArrayList<>();
    }


    /**
     * Makes each groups play in groups stages and qualify the winners to the final stage
     * @return List<Competitor> List of Competitor
     */
    private void playGroupesPhase() {
        
        this.groups = this.strategy.divideIntoGroups(this.competitorList);

        League league;
        List<Competitor> groupToPlay;
        Set<Map.Entry<Integer, List<Competitor>>> groupsEntry = this.groups.entrySet();
        Iterator<Map.Entry<Integer, List<Competitor>>> iterator = groupsEntry.iterator();
        while (iterator.hasNext()){
            Map.Entry<Integer, List<Competitor>> entry = iterator.next();
            groupToPlay = entry.getValue();
            this.display("Group No"+entry.getKey()+" ");
            league = new League(groupToPlay);
            league.play(groupToPlay);
            this.groupePhaseWiners = this.strategy.selectGroupWiners(groupToPlay);
            addToGroupsWinners(this.groupePhaseWiners);
        }
    }
  
    public void addToGroupsWinners(List<Competitor> groupePhaseWiners){
        for(int i = 0; i<groupePhaseWiners.size(); i++){
            this.finalPlayers.add(groupePhaseWiners.get(i));
        }
    }

    /**
     * @return
     * @throws ListSizeIsNotPowerOfTwoException
     * @throws EmptyCompetitorListException
     */
    public void playFinalPhase() throws EmptyCompetitorListException, ListSizeIsNotPowerOfTwoException {
        if(competitorList.isEmpty())
            throw new EmptyCompetitorListException("competitor's list is empty");
        if(isPowerOfTwo(competitorList)){
            Tournament tournament = new Tournament(this.finalPlayers);
            resetCompetitorsPoint(this.finalPlayers);
            tournament.play(this.finalPlayers);
            classification();
        }else{
            throw new ListSizeIsNotPowerOfTwoException("competitor's list size is not power of 2");
        
        }
    }

    /**
     * allows to play all the master matches .
     * @param competitors list of players
     * @throws ListSizeIsNotPowerOfTwoException throws an exception if Players nomber is not power of 2
     * @throws EmptyCompetitorListException
     */
    @Override
    public void play(List<Competitor> competitors) throws ListSizeIsNotPowerOfTwoException, EmptyCompetitorListException {
        if(isPowerOfTwo(competitorList)){
            display("de poules");
            this.playGroupesPhase();
            display("final");
            this.playFinalPhase();
        }else{
            throw new ListSizeIsNotPowerOfTwoException("Players nomber is not power of 2");
        }

    }

    private void display(String string) {
        System.out.println("*** Phase " + string + "***");
    }

    /**
     * Show the classification of the master
     * print the ranking of each group in the groupe phase and the ranking of the final phase
     */
    @Override
    public void classification() {
        this.finalPlayers.forEach(competitor -> {
            this.competitors.put(competitor, competitor.getPoints());
        });
    }
}


    
   
