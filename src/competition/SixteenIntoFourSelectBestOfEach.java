/**
 * 
 */
package src.competition;
import java.util.List;
import java.util.Map;

/**
 * @author Abdelaziz HAMMAD AHMED
 *
 */
public class SixteenIntoFourSelectBestOfEach extends MasterStrategy {

	/**
	 * 
	 */
    private final int NMBOFGROUPS = 4;
    private final int WINNERSOFGROUP = 1;

    /**
     * 
     */

	public SixteenIntoFourSelectBestOfEach() {
        super();
    }

    /**
     * Divides 16 players into 4 groups
     * @return Map<Integer, List<Competitor>
     */

    @Override
    public Map<Integer, List<Competitor>> divideIntoGroups(List<Competitor> competitors) {
        return divide(competitors, NMBOFGROUPS);
    }
    @Override
    public List<Competitor> selectGroupWiners(List<Competitor> group) {
        return selectCompetitors(group, WINNERSOFGROUP);
    }

 








    

}
