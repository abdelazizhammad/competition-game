package src.competition;

import java.util.ArrayList;
import java.util.List;

public class Journalists implements CompetitionObserver{
    private String channelName;
    protected static List<CompetitionObserver> observerList = new ArrayList<>();

    public Journalists(String channel){
        this.channelName = channel;
    }
    @Override
    public void watchMatch(Competitor c1, Competitor c2, Competitor winner) {
        
        System.out.println(" \n\n\n ");
        System.out.println(" *********************************************************** ");
        System.out.println(" Welcome everyone to "+this.channelName);
        System.out.println(" In today's match the competitors :");
        System.out.println(c1.toString()+" playing against " +c2.toString() );
        System.out.println(" and the winner is : "+winner.toString());
    }
	
}

