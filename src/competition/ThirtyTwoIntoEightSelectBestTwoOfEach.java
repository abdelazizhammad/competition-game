package src.competition;
import java.util.*;
/**
 * 
 */
public class ThirtyTwoIntoEightSelectBestTwoOfEach extends MasterStrategy {
    /**
     * 
     */
    private final int NMBOFGROUPS = 8;
    private final int WINNERSOFGROUP  = 2;
    /**
     * Default constructor
     */
    public ThirtyTwoIntoEightSelectBestTwoOfEach() {
    }



    

    /**
     * Divides 32 players into 8 groups
     * @return Map<Integer, List<Competitor>
     */

    @Override
    public Map<Integer, List<Competitor>> divideIntoGroups(List<Competitor> competitors) {
        return divide(competitors, NMBOFGROUPS);
    }

    /**
     * @param group
     * @return
     */
    @Override
    public List<Competitor> selectGroupWiners(List<Competitor> group) {
        return selectCompetitors(group, WINNERSOFGROUP);
    }






}
