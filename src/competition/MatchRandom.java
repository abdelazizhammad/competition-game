package src.competition;
import java.util.Random;


/**
 * 
 * MatchRandom class
 *
 */
public class MatchRandom implements Match {
	/**
	 * Constructor for MatchRandom
	 */
  
    public MatchRandom(){}
    /**
	 * @param competitor1
	 * @param competitor2
	 * return winner
	 */
 @Override
	public Competitor playMatch(Competitor competitor1, Competitor competitor2) {
	        Competitor winner;
			Random ran = new Random();
	        int random = ran.nextInt();
	        if(random > 0){
	            competitor1.addPoint();
	            winner = competitor1;
	        }
	        else {
	            competitor2.addPoint();
	            winner = competitor2;
	        }
        return winner;
    }

  
}
