package src.competition;
public interface CompetitionObserver {
    void watchMatch(Competitor c1, Competitor c2, Competitor winner);
}
