package src.competition;
import java.util.*;


public class CompetitionMain {
    public static void play(Competition competition){
        try{
            competition.play();
            System.out.println(" Ranking ");
            Set<Map.Entry<Competitor, Integer>> competitorEntry = competition.ranking().entrySet();
            Iterator<Map.Entry<Competitor, Integer>> iterator = competitorEntry.iterator();
            while (iterator.hasNext()){
                Map.Entry<Competitor, Integer> entry = iterator.next();
                System.out.println(entry.getKey().toString() + "-" + entry.getValue());
            }
        }catch (EmptyCompetitorListException | ListSizeIsNotPowerOfTwoException except){
            System.out.println(except.getMessage());
        }
    }
 
    public static void main(String []args){
        Competition competition;
        List<Competitor> competitorList = new ArrayList<>();
        for(int i=1; i<args.length; i++){
            Competitor competitor = new Competitor(args[i]);
            competitorList.add(competitor);
        }
        switch (args[0]){
            case "League":
                competition = new League(competitorList);
                competition.addObserver(new Journalists("Canal+"));
                competition.addObserver(new BookMakers("winamax", competitorList));
                play(competition);
                break;
            case "Tournament":
                competition = new Tournament(competitorList);
                competition.addObserver(new Journalists("Canal+"));
                competition.addObserver(new BookMakers("ParionsSport", competitorList));               
                play(competition);
                break;
	        case "SixteenIntoFourSelectBestOfEach":
                SixteenIntoFourSelectBestOfEach strategy = new SixteenIntoFourSelectBestOfEach();
	            competition = new Master(competitorList, strategy);
                competition.addObserver(new Journalists("Canal+"));
                competition.addObserver(new BookMakers("Bet+", competitorList));
	            play(competition);
	            break;
            case "SixteenIntoFourSelectBestTwoOfEach":
	            MasterStrategy strategy1 = new SixteenIntoFourSelectBestTwoOfEach();
	            competition = new Master(competitorList, strategy1);
                competition.addObserver(new Journalists("Canal+"));
                competition.addObserver(new BookMakers("Nelson Montfort ", competitorList));
	            play(competition);
	            break;
            
	        case "TwentyFourIntoThreeGroups":
                MasterStrategy strategy2 = new TwentyFourIntoThreeGroups();
                competition = new Master(competitorList, strategy2);
                competition.addObserver(new Journalists("Canal+"));
                competition.addObserver(new BookMakers("winamax ", competitorList));	                
                play(competition);
	            break;
	        case "ThirtyTwoIntoEightSelectBestTwoOfEach":
	            for(int i=1; i<=32; i++){
	                Competitor competitor = new Competitor("Team"+i);
	                competitorList.add(competitor);
	            }
                MasterStrategy strategy4 = new ThirtyTwoIntoEightSelectBestTwoOfEach();	                
                competition = new Master(competitorList, strategy4);
                competition.addObserver(new Journalists("Canal+ "));
                competition.addObserver(new BookMakers("ParionsSport ", competitorList));
	            play(competition);
                break;
                
            




                
        }

    }
}
