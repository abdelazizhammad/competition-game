package src.competition;

/** 
 * This class is used to represent a competitor .
*/
public class Competitor{

	/** The name of the competitor */
	private String name;

	/** The number of the points for a competitor */
	private int points;


	

	/**
	 * constructor of a competitor.
	 */
	public Competitor(String name) {
		this.name = name;
		this.points = 0;
	
	}
	
	/** this methods returns the competitor's name.
	 * @return String competitor's name.
	 */
	public String getName() {
		return this.name;
		
	}
	
	/** This method returns the points of competitor.
	 * @return Int the point of competitor.
	 */
	public int getPoints() {
		return this.points;
		
	}
	
	/** This method is to add point to the competitor.
	* @param point the number of point to add. 
	 */
	public void addPoint() {
		this.points += 1;
		
	}
	
	/** This method is to reset competitor's points to 0 
	 */
	public void resetPoints(){
		this.points = 0;
	}

	public boolean equals(Object object){
        if(object instanceof Competitor){
            Competitor other = (Competitor)object;
            return this == other;
        }
        return false;
    }
    
	public String toString() {
		return  this.name; 
	}

	



}
