package src.competition;
import java.util.*;

public abstract class MasterStrategy implements Strategy{
    private Map<Integer, List<Competitor>> groupStageTeams;
    private List<Competitor> buffer; 
    
   /**
    * Default constructor
    */
   public MasterStrategy() {
       this.groupStageTeams  = new HashMap<>();
       this.buffer = new ArrayList<>();
  
   }   

    /**
     * @param competitors 
     * @param nmbOfGroups 
     * 
     * @return
     */
   
    
    public Map<Integer, List<Competitor>> divide(List<Competitor> competitors, int nmbOfGroups) {
    	Integer groupKey = 1;
    	for(int i = 0; i < competitors.size();i++) {
    		this.buffer.add(competitors.get(i));
    		if(this.buffer.size()== competitors.size()/nmbOfGroups) {
				this.groupStageTeams.put(groupKey,(this.buffer));
    			this.buffer = new ArrayList<>();
				groupKey++;   			
    		}
    	}
       return this.groupStageTeams;
    }

    /**
     * return the best competitor in the group
     * @param group the group of competitors
     * @return Competitor
     */
    public Competitor selectBestCompetitor(List<Competitor> group){
        Competitor comp = group.get(0);
        for(int i = 1; i<group.size(); i++){
            if(comp.getPoints() < group.get(i).getPoints()){
                comp = group.get(i);
            }else if(comp.getPoints() == group.get(i).getPoints()){
                int random = new Random().nextInt(2);
                comp = group.get(random);
            }
        }
        return comp;
    }


    /**
     * return the a defined nomber of selected winners of competitors in the group
     * @param group group of the competitors
     * @param groupWiners the nomber of winers from each list of group of the competitors
     * @return ArrayList
     */
    public List<Competitor> selectCompetitors(List<Competitor> group,int groupWiners){
        List<Competitor> best = new ArrayList<>();
        while (best.size() < groupWiners){
            Competitor bestComp = selectBestCompetitor(group);
            best.add(bestComp);
            group.remove(bestComp);
        }
        return best;
    }



    abstract public Map<Integer, List<Competitor>> divideIntoGroups(List<Competitor> competitorList);

    abstract public List<Competitor> selectGroupWiners(List<Competitor> groupToPlay);



    

}
