package src.competition;
import java.util.*;
public interface Strategy {
  
	    /**
	     * @param competitors 
	     * @return
	     */
	    abstract public Map<Integer, List<Competitor>> divideIntoGroups(List<Competitor> competitors);
        abstract public List<Competitor> selectGroupWiners(List<Competitor> group);
    

}
