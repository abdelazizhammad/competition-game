# Projet COO : Compétitions Sportives
  * version 03 (livrable 3)
## AUTHOR:
  * Abdelaziz HAMMAD AHMED

## Introduction
  * Une compétition sportive est définie par un ensemble de matchs (Match) joués entre compétiteurs (Competitor). La compétition est responsable de l’organisation de ses matchs.
On ajoute un nouveau type de compétition, les Master, qui font s’affronter les compétiteurs en deux phases. La
compétition commence par une phase de poules à l’issue de laquelle certains compétiteurs sont sélectionnés pour dis-
puter la phase finale du tournoi. Les poules sont organisées sour forme de championnat tandis que la phase finale se
déroule sous la forme d’un tournoi à élimination directe.
Les competitions sont desormais si populaires que les media et les parieurs s’y interessent. Des journalistes peuvent donc assister aux competitions et diffuser les resultats des matchs.

## HowTo
  * Récupération du dépôt avec les 2 commndes suivants:
      
    Avec SSH :
      `git clone git@gitlab-etu.fil.univ-lille.fr:abdelaziz.hammadahmed.etu/hammad-coo-project.git`
     
    Avec HTTPS :
      `git clone https://gitlab-etu.fil.univ-lille.fr/abdelaziz.hammadahmed.etu/hammad-coo-project.git`

## Compiler tout le dossier (classes) :
  * Placez-vous dans la racine du project 
	  `cd hammad-coo-project/`
  
  * puis pour compiler, faites la commande 
   	`javac -encoding utf8 -d class -cp class src/competition/*.java src/util/*.java`


## Générer la documentation :
  
  * Placez-vous dans  
    `cd hammad-coo-project/`

  * puis pour compiler, faites la commande 
    `javadoc -d  doc  -cp doc src/competition/*.java src/util/*.java`


## creation du jar

  * Placez-vous dans la dossier `class` en lançant le commande 
    `cd class`

  * puis faites la commande
    `jar cvmf ../manifest-ex ../competition.jar src/competition/*.class src/util/*.class`


## Consulter documentation :
  * Ouvrir le fichier index.html puis pacourir tous les fichiers.


## Compilation des tests

  * Placez vous dans le dossier
    `cd hammad-coo-project`
  
  * puis faites la commande :
    `javac -d class -cp class  -classpath junit-platform-console-standalone-1.9.1.jar test/*.java src/competition/*.java src/util/*.java`

## Lancer le jeu

  * Placez vous dans la racine du project 
    
    `cd hammad-coo-project`

  * puis, lancer les commandes :
    * pour la league : 
      
      `java -jar competition.jar  League Abdelaziz John Juan  Nancy Prakash Zidane `

    * pour le tournament: 
      
      `java -jar competition.jar  Tournament Abdelaziz Jean John Juan Sara Nancy Prakash Zidane`
    
    * pour les masters :
      
      `java -jar competition.jar SixteenIntoFourSelectBestOfEach c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15`

      `java -jar competition.jar SixteenIntoFourSelectBestTwoOfEach c0 c1 c2 c3 c4 c5 c6 c7 c8 c9 c10 c11 c12 c13 c14 c15`

      `java -jar competition.jar ThirtyTwoIntoEightSelectBestTwoOfEach`



## Element de code:
  ** Mise en pratique des principes :
   - SOLID.
   - Open closed principle sur les types de compétitions et les types de match. 
   - TDD.

## Remarque :
  Le type de compétition (League, Tournament) peut étre passer sur la ligne de commnde ainsi que les noms des Competitors.
  Pour la dernière stratégie de 32 jouers j'ai initialisé les noms de joueurs dans le main donc n'est perdre du temps à taper le nom d'équipes il suffit donc de juste le paramètre `ThirtyTwoIntoEightSelectBestTwoOfEach` puis Entrez.
## Diagramme UML :
![Screenshot](modelisation/competition02.png)
![Screenshot](modelisation/uml_competition.png)
![Screenshot](modelisation/uml_util.png)


